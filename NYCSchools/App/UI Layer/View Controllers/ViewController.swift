//
//  ViewController.swift
//  NYCSchools
//
//  Created by Rolan on 8/1/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let api : SchoolAPILogic = SchoolAPI()
        
        api.getSchools() { response in
            
            switch response {
            case .failure(let error):
                print("error occur \(error.localizedDescription)")
            case .success(let data):
                print("the total length of data is \(data.count)")
            }
        }
    }
}

