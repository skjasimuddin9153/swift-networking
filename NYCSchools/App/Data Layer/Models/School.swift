//
//  School.swift
//  NYCSchools
//
//  Created by Techwens on 03/04/24.
//

import Foundation

struct School : Codable {
    let dbn: String
    let schoolName: String
    let numOfSATTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
    
    enum CodingKeys : String , CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numOfSATTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        
    }
}
