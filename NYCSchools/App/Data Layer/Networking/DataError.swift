//
//  DataError.swift
//  NYCSchools
//
//  Created by Techwens on 03/04/24.
//

import Foundation

enum DataError : Error {
    case networkingError(String)
}
