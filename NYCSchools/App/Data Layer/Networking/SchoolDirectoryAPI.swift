//
//  SchoolDirectoryAPI.swift
//  NYCSchools
//
//  Created by Rolan on 8/8/22.
//


typealias SchoolComplitation = (Swift.Result<[School] , DataError>) -> Void

import Foundation
import Alamofire
protocol SchoolAPILogic {
    func getSchools(completion: @escaping (SchoolComplitation))
}

class SchoolAPI: SchoolAPILogic {
    
    private  struct Constants {
      static  let SchoolListUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    func getSchools(completion: @escaping (SchoolComplitation)) {
        let url = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        // TODO: implement me
        print("imeplement get schools")
        getDataUsingAF(completion: completion)
//        retriveSchoolusingStandradService()
    }
    
//    MARK: - Using Almafire
    
    
    private func getDataUsingAF(completion: @escaping (SchoolComplitation)){
        
        AF.request(Constants.SchoolListUrl)
            .validate()
            .responseDecodable(of: [School].self) { response in
                switch response.result {
                case .failure(let error):
                    completion(.failure(.networkingError(error.localizedDescription)))
                    break
                case .success(let data):
                    completion(.success(data))
                    break
                }
            }
    }
    
    
    
//    MARK: - Using Simple Url Session
    private func retriveSchoolusingStandradService(){
        var urlComponent = URLComponents()
        urlComponent.scheme = "https"
        urlComponent.host = "data.cityofnewyork.us"
        urlComponent.path = "resource/f9bf-2cp4.json"
//        urlComponent.queryItems = [URLQueryItem(name: "__key__", value: "__value__")]
        
        
//        another way to get url
//        URL(string:schoolUrl)
        if let url = urlComponent.url {
            let urlSession = URLSession(configuration: .default)
            
            let task = urlSession.dataTask(with: url) { data, response, error in
                guard  error == nil else {
                    print("error occur \(error?.localizedDescription)")
                    return
                }
                
                if let data = data {
                    let decoder = JSONDecoder()
                    
                    do {
                        let school:[School] = try decoder.decode([School].self, from: data)
                        
                        print("data loded success \(school)")
                    } catch let error {
                        
                        print("error during parsing \(error.localizedDescription)")
                    }
                   
                }
                
            }
            
            task.resume()
        }
    }
}
